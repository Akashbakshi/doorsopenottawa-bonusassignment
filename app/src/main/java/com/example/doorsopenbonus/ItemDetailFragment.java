package com.example.doorsopenbonus;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.doorsopenbonus.model.DoorsOpenContent;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private DoorsOpenContent.DOBuilding mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = DoorsOpenContent.itemmap.get(getArguments().getString(ARG_ITEM_ID));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(getResources().getString(R.string.title_item_detail));
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);


        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.detailDescription)).setText(mItem.buildingDescription);
            ((TextView) rootView.findViewById(R.id.detailBuildingName)).setText(mItem.buildingName);
            ((TextView) rootView.findViewById(R.id.detailCategory)).setText(mItem.buildingCategory);
            ((TextView) rootView.findViewById(R.id.tvSatHours)).setText(mItem.buildingSatHours);
            ((TextView) rootView.findViewById(R.id.tvSunHours)).setText(mItem.buildingSunHours);
            if(mItem.buildingSatHours.equals("Closed Saturday"))
                ((ImageView) rootView.findViewById(R.id.satImgView)).setBackground(getResources().getDrawable(R.drawable.ic_ban_solid));


            if(mItem.buildingSunHours.equals("Closed Sunday"))
                ((ImageView) rootView.findViewById(R.id.sunImgView)).setBackground(getResources().getDrawable(R.drawable.ic_ban_solid));
            ((TextView) rootView.findViewById(R.id.websiteLB)).setText(mItem.buildingWebsite);

            Picasso.get().load(mItem.buildingImg).into((ImageView) rootView.findViewById(R.id.detailBuildingImg));




        }

        return rootView;
    }
}
