package com.example.doorsopenbonus.model;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;


public class DoorsOpenContent {
    public static final List<DOBuilding> items = new ArrayList<DOBuilding>();
    public static final Map<String,DOBuilding> itemmap = new HashMap<String, DOBuilding>(); // Will hold all our DOBuilding Models mapped by their index

    static{
        runGETRequest(); // Run our GET Request on start up
    }


    public static void addBuilding(DOBuilding building){
        items.add(building);
        itemmap.put(building.id, building);
    }


    public static void runGETRequest(){
        try {
            String results = new HttpGetRequest().execute().get(); // Use HTTP GET Request to get elements from API

            JSONArray res = new JSONArray(results); // Convert results to JSON Array


            Log.d("JSON",res.toString());

            if(res.length()>0){ // only execute if we received results
                for(int i = 0; i < res.length(); i++) {

                    ArrayList tmpAmen = new ArrayList(); // create the temp arraylist that will hold all the amenities for each element

                    String isAccessible = res.getJSONObject(i).get("isAccessible").toString();
                    String hasBikeParking = res.getJSONObject(i).get("isBikeParking").toString();
                    String isFamilyFriendly = res.getJSONObject(i).get("isFamilyFriendly").toString();
                    String hasGuidedTour = res.getJSONObject(i).get("isGuidedTour").toString();
                    String hasOCTranspo = res.getJSONObject(i).get("isOCTranspoNearby").toString();
                    String isOpenSunday = res.getJSONObject(i).get("isOpenSunday").toString();
                    String isOpenSaturday = res.getJSONObject(i).get("isOpenSaturday").toString();
                    String hasFreeParking = res.getJSONObject(i).get("isFreeParking").toString();
                    String hasPaidParking = res.getJSONObject(i).get("isPaidParking").toString();
                    String hasPublicParking = res.getJSONObject(i).get("isPublicWashrooms").toString();
                    String hasShuttle = res.getJSONObject(i).get("isShuttle").toString();


                    //Add all results to our list
                    tmpAmen.add(isAccessible);
                    tmpAmen.add(hasBikeParking);
                    tmpAmen.add(isFamilyFriendly);
                    tmpAmen.add(hasGuidedTour);
                    tmpAmen.add(hasOCTranspo);
                    tmpAmen.add(isOpenSaturday);
                    tmpAmen.add(isOpenSunday);
                    tmpAmen.add(hasFreeParking);
                    tmpAmen.add(hasPaidParking);
                    tmpAmen.add(hasPublicParking);
                    tmpAmen.add(hasShuttle);


                    String imgURL = "https://doors-open-ottawa-ro.mybluemix.net/"+res.getJSONObject(i).get("image").toString(); // Build the URL to get the image


                    String sundayHours = "";
                    String saturdayHours= "";

                    // Logic to populate Date Strings with the correct format as per requirement
                    if(isOpenSaturday.equals("true") && isOpenSunday.equals("false")) {
                        String satOpen =  res.getJSONObject(i).getString("saturdayStart");
                        String satClose =  res.getJSONObject(i).getString("saturdayClose");
                        SimpleDateFormat satStart = new SimpleDateFormat("yyyy-mm-dd hh:mm");
                        satStart.setLenient(true);
                        Date satStartDate = satStart.parse(satOpen);
                        Date satEndDate =  satStart.parse(satClose);
                        SimpleDateFormat properDate = new SimpleDateFormat("EEEE, MMMM d, hh:mm a");
                        SimpleDateFormat hrs = new SimpleDateFormat("hh:mm a");

                        saturdayHours = properDate.format(satStartDate)+ " - "+hrs.format(satEndDate);
                        sundayHours = "Closed Sunday";
                    }
                    else if(isOpenSaturday.equals("false") && isOpenSunday.equals("true")) {
                        String sunOpen =  res.getJSONObject(i).getString("sundayStart");
                        String sunClose =  res.getJSONObject(i).getString("sundayClose");
                        SimpleDateFormat sunStart = new SimpleDateFormat("yyyy-mm-dd HH:mm");
                        SimpleDateFormat sunEnd = new SimpleDateFormat("yyyy-mm-dd HH:mm");
                        Date sunStartDate = sunStart.parse(sunOpen);
                        Date sunEndDate = sunEnd.parse(sunClose);

                        SimpleDateFormat properDate = new SimpleDateFormat("EEEE, MMMM d, hh:mm a");
                        SimpleDateFormat hrs = new SimpleDateFormat("hh:mm a");

                        sundayHours = properDate.format(sunStartDate)+ " - "+hrs.format(sunEndDate);
                        saturdayHours = "Closed Saturday";
                    }
                    else if(isOpenSaturday.equals("true") && isOpenSunday.equals("true")){

                        String satOpen =  res.getJSONObject(i).getString("saturdayStart");
                        String satClose =  res.getJSONObject(i).getString("saturdayClose");
                        SimpleDateFormat satStart = new SimpleDateFormat("yyyy-mm-dd HH:mm");
                        SimpleDateFormat satEnd = new SimpleDateFormat("yyyy-mm-dd HH:mm");
                        Date satStartDate = satStart.parse(satOpen);
                        Date satEndDate =  satEnd.parse(satClose);

                        SimpleDateFormat properDate = new SimpleDateFormat("EEEE, MMMM d, hh:mm a");
                        SimpleDateFormat hrs = new SimpleDateFormat("hh:mm a");
                        saturdayHours = properDate.format(satStartDate)+ " - "+hrs.format(satEndDate);

                        String sunOpen =  res.getJSONObject(i).getString("sundayStart");
                        String sunClose =  res.getJSONObject(i).getString("sundayClose");
                        SimpleDateFormat sunStart = new SimpleDateFormat("yyyy-mm-dd HH:mm");
                        SimpleDateFormat sunEnd = new SimpleDateFormat("yyyy-mm-dd HH:mm");
                        sunStart.parse(sunOpen);
                        sunEnd.parse(sunClose);
                        Date sunStartDate = sunStart.parse(sunOpen);
                        Date sunEndDate = sunEnd.parse(sunClose);
                        sundayHours = properDate.format(sunStartDate)+ " - "+hrs.format(sunEndDate);
                    }
                    else if(isOpenSaturday.equals("false") && isOpenSunday.equals("false")) {
                        saturdayHours = "Closed Saturday";
                        sundayHours = "Closed Sunday";
                    }

                    Log.d("Vals",res.getJSONObject(i).toString());
                    addBuilding(new DOBuilding(res.getJSONObject(i).get("buildingId").toString(),res.getJSONObject(i).get("nameEN").toString(),imgURL,res.getJSONObject(i).get("addressEN").toString(),res.getJSONObject(i).get("descriptionEN").toString(),res.getJSONObject(i).get("categoryEN").toString(),saturdayHours,sundayHours,res.getJSONObject(i).get("websiteEN").toString(),tmpAmen));

                }
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }


    public static class HttpGetRequest extends AsyncTask<String,Void,String>{
        @Override
        protected String doInBackground(String... params) {
            String results = "";
            String inputLine = "";
            URL doURL = null;

            try {
                doURL = new URL("https://doors-open-ottawa-ro.mybluemix.net/buildings");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                HttpsURLConnection connection = (HttpsURLConnection) doURL.openConnection();
                connection.setRequestMethod("GET");
                connection.setReadTimeout(15000);
                connection.setConnectTimeout(15000);

                connection.connect();

                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());


                //Create a new buffered reader and String Builder
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder(); // Make string builder to read the request line by line

                //Check if the line we are reading is not null
                while((inputLine = reader.readLine()) != null){
                    stringBuilder.append(inputLine);
                }

                //Close our InputStream and Buffered reader
                reader.close();
                streamReader.close();

                //Set our result equal to our stringBuilder
                results = stringBuilder.toString();

                JSONObject res = new JSONObject(results); // Convert the results of our request to a JSONObject

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return results;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
        }
    }
    public static class DOBuilding{
        public String id;
        public String buildingName;
        public String buildingImg;
        public String buildingAddress;
        public String buildingDescription;
        public String buildingCategory;
        public String buildingSatHours;
        public String buildingSunHours;
        public String buildingWebsite;

        public List<String> buildingAmenities = new ArrayList<>();


        public DOBuilding(String id,String buildingName,String buildingIMG,String buildingAddress,String buildingDescription,String buildingCategory,String buildingSatHr, String buildingSunHr,String bWebsite , ArrayList<String> amenities){
            this.id = id;
            this.buildingName = buildingName;
            this.buildingImg = buildingIMG;
            this.buildingAddress = buildingAddress;
            this.buildingDescription = buildingDescription;
            this.buildingCategory = buildingCategory;
            this.buildingSatHours = buildingSatHr;
            this.buildingSunHours = buildingSunHr;
            this.buildingWebsite = bWebsite;
            this.buildingSatHours = buildingSatHr;
            this.buildingSunHours = buildingSunHr;

            this.buildingAmenities = amenities;
        }

    }
}

