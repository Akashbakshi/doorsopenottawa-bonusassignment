package com.example.doorsopenbonus;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Debug;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.doorsopenbonus.model.DoorsOpenContent;

import static com.example.doorsopenbonus.ItemDetailFragment.ARG_ITEM_ID;

/**
 * An activity representing a single Item detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link ItemListActivity}.
 */
public class ItemDetailActivity extends AppCompatActivity {
    final String ARG_ITEM_ID = "item_id";
    DoorsOpenContent.DOBuilding mItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);



        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        LinearLayout bottomSheetLL = findViewById(R.id.bottom_sheet);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetLL);

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED); // Set state to collapsed so it is not expanded


// set the peek height
        bottomSheetBehavior.setPeekHeight(100);

// set hideable or not
        bottomSheetBehavior.setHideable(false);


        Bundle bundle = getIntent().getExtras();

        String arg = bundle.getString(ARG_ITEM_ID); // Get String for our argument ID

        mItem = DoorsOpenContent.itemmap.get(arg); // Get the Model object being passed

        Drawable isAvailable = getResources().getDrawable(R.drawable.ic_check_circle_regular);
        if (mItem != null) {
            // Set Amenities Icons
            if(mItem.buildingAmenities.get(0).equals("true"))
                ((ImageView)findViewById(R.id.iconAccessible)).setBackground(isAvailable);
            if(mItem.buildingAmenities.get(1).equals("true"))
                ((ImageView)findViewById(R.id.iconBikeParking)).setBackground(isAvailable);
            if(mItem.buildingAmenities.get(2).equals("true"))
                ((ImageView)findViewById(R.id.iconFamilyFriendly)).setBackground(isAvailable);
            if(mItem.buildingAmenities.get(3).equals("true"))
                ((ImageView)findViewById(R.id.iconGuidedTour)).setBackground(isAvailable);
            if(mItem.buildingAmenities.get(4).equals("true"))
                ((ImageView)findViewById(R.id.iconOCTranspo)).setBackground(isAvailable);
            if(mItem.buildingAmenities.get(5).equals("true"))
                ((ImageView)findViewById(R.id.iconOpenSat)).setBackground(isAvailable);
            if(mItem.buildingAmenities.get(6).equals("true"))
                ((ImageView)findViewById(R.id.iconOpenSun)).setBackground(isAvailable);
            if(mItem.buildingAmenities.get(7).equals("true"))
                ((ImageView)findViewById(R.id.iconFreeParking)).setBackground(isAvailable);
            if(mItem.buildingAmenities.get(8).equals("true"))
                ((ImageView)findViewById(R.id.iconPaidParking)).setBackground(isAvailable);
            if(mItem.buildingAmenities.get(9).equals("true"))
                ((ImageView)findViewById(R.id.iconPublicWashroom)).setBackground(isAvailable);
            if(mItem.buildingAmenities.get(10).equals("true")) {
                ((ImageView) findViewById(R.id.iconShuttle)).setBackground(isAvailable);
            }
        }
        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(ARG_ITEM_ID,
                    getIntent().getStringExtra(ARG_ITEM_ID));
            ItemDetailFragment fragment = new ItemDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            navigateUpTo(new Intent(this, ItemListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
